const { Board } = require('../models/board');
const { cloudinary } = require("../cloudinary");
const stringToBoolean = require('../utils/stringToBoolean');


module.exports.index = async (req, res) => {
    const pP = 5;
    const { p = 1, q = "_none", s = "new" } = req.query;
    let searchQuery = {};
    let sortParams = {};
    let pageTitle = "All Boards"

    // determines whether it shows index filtered to your likes, your boards, or all
    switch (q) {
        case "_liked":
            searchQuery = { likes: req.user._id }
            pageTitle = "Liked Boards"
            res.locals.currentNav = "explore"
            break;
        case "_me":
            searchQuery = { author: req.user._id }
            res.locals.currentNav = "create"
            pageTitle = "Your Boards"
            break;
        case "_none":
            searchQuery = { published: true }
            res.locals.currentNav = "explore"
            pageTitle = "All Boards"
            break;
        default:
            searchQuery = { published: true, title: { "$regex": q, "$options": "i" } }
            res.locals.currentNav = "explore"
            pageTitle = "Search Results"
            break;
    }
    // determines the sort method
    switch (s) {
        case "likes":
            sortParams = { numlikes: "desc" };
            break;
        case "plays":
            sortParams = { timesplayed: "desc" };
            break;
        case "new":
        default:
            sortParams = { submitted: "desc" };
            break;
    }

    const boards = await Board.find(searchQuery).sort(sortParams).limit(pP).skip(pP * (p - 1))
        .populate({
            path: 'comments',
            populate: {
                path: 'author'
            }
        }).populate('author');


    for (let board of boards) {
        if (req.user) {
            board.userLikeIndex = board.getUserLikeIndex(req.user._id);
        } else {
            board.userLikeIndex = -1;
        }
    }

    res.render('boards/index', { boards, p, pP, q, s, pageTitle })
}

module.exports.renderNewForm = (req, res) => {
    const board = {
        new: true,
        _id: 0,
        title: "",
        description: "",
        published: false,
        images: [],
    };
    res.render('boards/edit', { board, currentNav: "create" });
}

module.exports.createBoard = async (req, res, next) => {
    console.log(`Creating board:`)

    const board = new Board(req.body.board);
    board.likes.push(res.locals.currentUser._id);
    board.numlikes = 1;
    board.submitted = new Date();
    console.log("req files")
    console.log(req.files)
    board.images = req.files.map(f => ({ url: f.path, filename: f.filename }));

    console.log(req.body.nameImages)
    if (req.body.nameImages) {
        req.body.nameImages.forEach(function (imgName, i) {
            board.images[i].name = imgName;
        });
    }

    board.author = req.user._id;
    board.published = false;
    await board.save();
    req.flash('success', 'Successfully made a new board!');
    res.redirect(`/boards/${board._id}/edit`)
}

module.exports.showBoard = async (req, res,) => {
    const board = await Board.findById(req.params.id).populate({
        path: 'comments',
        populate: {
            path: 'author'
        }
    }).populate('author');
    if (!board) {
        req.flash('error', 'Cannot find that board!');
        return res.redirect('/boards');
    }
    if (req.user) {
        board.userLikeIndex = board.getUserLikeIndex(req.user._id);
    } else {
        board.userLikeIndex = -1;
    }
    res.render('boards/show', { board, currentNav: "explore" });
}

module.exports.renderEditForm = async (req, res) => {
    const { id } = req.params;
    const board = await Board.findById(id)
    if (!board) {
        req.flash('error', 'Cannot find that board!');
        return res.redirect('/boards');
    }
    res.render('boards/edit', { board, currentNav: "create" });
}

module.exports.updateBoard = async (req, res) => {
    const { id } = req.params;
    console.log(`Updating board: ${id}`)

    req.body.board.published = stringToBoolean(req.body.board.published);
    const board = await Board.findByIdAndUpdate(id, { ...req.body.board });
    // delete selected images
    if (req.body.deleteImages) {
        for (let filename of req.body.deleteImages) {
            await cloudinary.uploader.destroy(filename);
        }
        await board.updateOne({ $pull: { images: { filename: { $in: req.body.deleteImages } } } })
    }
    // push in new images
    board.images.push.apply(board.images, req.files.map(f => ({ url: f.path, filename: f.filename })));

    // apply names to images
    req.body.nameImages.forEach(function (imgName, i) {
        board.images[i].name = imgName;
    });

    // save board
    await board.save();

    // lazily check if the board has enough images to be published
    const checkBoard = await Board.findById(id);
    if (checkBoard.images.length !== 24) {
        checkBoard.published = false;
        await checkBoard.save();
    }

    req.flash('success', 'Successfully updated board!');
    res.redirect(`/boards/${board._id}`)
}

module.exports.toggleLike = async (req, res) => {
    const { id } = req.params;
    const likedIndex = res.locals.boardLiked;
    if (likedIndex == -1) {
        const board = await Board.findByIdAndUpdate(id,
            {
                $push: { likes: res.locals.currentUser._id }
            }
        );
        board.numlikes = board.likes.length;
        await board.save();

        req.flash('success', 'Liked board!')
        res.redirect(`/boards/${board._id}`)
    } else {
        const board = await Board.findByIdAndUpdate(id,
            {
                $pull: { likes: res.locals.currentUser._id }
            }
        );
        board.numlikes = board.likes.length;
        await board.save();

        req.flash('success', 'Unliked board!')
        res.redirect(`/boards/${board._id}`)
    }
}

module.exports.flagBoard = async (req, res) => {
    const { id } = req.params;
    console.log("flagging board")
    const board = await Board.findByIdAndUpdate(id, { flagged: true, flaggedBy: req.user._id });
    req.flash('success', 'Reported board')
    res.redirect(`/boards`);
}

module.exports.unflagBoard = async (req, res) => {
    const { id } = req.params;
    const board = await Board.findByIdAndUpdate(id, { flagged: false, flaggedBy: null });
    req.flash('success', 'Unflagged board')
    res.redirect('/admin');
}

module.exports.deleteBoard = async (req, res) => {
    const { id } = req.params;
    const board = await Board.findById(id);
    if (board.images.length > 0) {
        for (let image of board.images) {
            await cloudinary.uploader.destroy(image.filename);
        }
    }
    await Board.findByIdAndDelete(id);
    req.flash('success', 'Successfully deleted board')
    res.redirect('/boards');
}