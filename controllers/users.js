const { User } = require('../models/user');
const encodeImage = require('../utils/encodeImage');
const dateAdd = require('../utils/dateAdd');
const randomstring = require("randomstring");
const nodemailer = require("nodemailer");

async function sendEmail(messageObj) {
    // let testAccount = await nodemailer.createTestAccount();
    let transporter = nodemailer.createTransport({
        host: "smtp.office365.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASS,
        },
        requireTLS: true,
        tls: {
            ciphers: 'SSLv3'
        }
    });
    let info = await transporter.sendMail({
        from: `"No Reply" <${process.env.SMTP_FROM}>`, // sender address
        to: messageObj.to, // list of receivers
        subject: messageObj.subject, // Subject line
        text: messageObj.bodyText, // plain text body
        html: messageObj.bodyHtml, // html body
    });
    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

}

async function sendVerificationEmail(userName, emailAddress, verificationToken) {
    await sendEmail({
        to: emailAddress,
        subject: "Email Verification for Who Is It",
        bodyText: `Hello ${userName},
        \r\n\r\nThis email address has been added to your Who Is It account. Before you can start building game boards, please verify your email address.
        \r\n\r\nhttps://whoisit.tetrayoubetra.com/users/verify/${verificationToken}
        \r\n\r\nIf ${userName} is not your Who Is It account, let us know and we'll make sure nothing weird is going on.
        \r\n\r\nWho Is It?!
        \r\nhttps://whoisit.tetrayoubetra.com/`,
        bodyHtml: `Hello ${userName},
        <p>This email address has been added to your Who Is It account. Before you can start building game boards, please verify your email address.</p>
        <p style="text-align: center"><a href="https://whoisit.tetrayoubetra.com/users/verify/${verificationToken}">https://whoisit.tetrayoubetra.com/users/verify/${verificationToken}</a></p>
        <p>If ${userName} is not your Who Is It account, let us know and we'll make sure nothing weird is going on.</p>
        <p style="text-align: center"><a href="https://whoisit.tetrayoubetra.com/"><img src="data:image/png;base64,${encodeImage('./public/img/logo.png')}" alt="Who Is It Logo">
        <br>https://whoisit.tetrayoubetra.com/</a></p>`
    });
}

async function sendResetEmail(userName, emailAddress, resetToken) {
    await sendEmail({
        to: emailAddress,
        subject: "Password Reset for Who Is It",
        bodyText: `Hello ${userName},
        \r\n\r\nA password reset has been requested for your account. The link below will take you to a password reset form.
        \r\n\r\nhttps://whoisit.tetrayoubetra.com/users/forgot/${resetToken}
        \r\n\r\nIf ${userName} is not your Who Is It account or you did not initiate this reset, delete this message and let us know if you feel anything fishy is occurring.
        \r\n\r\nWho Is It?!
        \r\nhttps://whoisit.tetrayoubetra.com/`,
        bodyHtml: `Hello ${userName},
        <p>A password reset has been requested for your account. The link below will take you to a password reset form.</p>
        <p style="text-align: center"><a href="https://whoisit.tetrayoubetra.com/users/forgot/${resetToken}">https://whoisit.tetrayoubetra.com/users/forgot/${resetToken}</a></p>
        <p>If ${userName} is not your Who Is It account or you did not initiate this reset, delete this message and let us know if you feel anything fishy is occurring.</p>
        <p style="text-align: center"><a href="https://whoisit.tetrayoubetra.com/"><img src="data:image/png;base64,${encodeImage('./public/img/logo.png')}" alt="Who Is It Logo">
        <br>https://whoisit.tetrayoubetra.com/</a></p>`
    });
}

async function sendPasswordChangeEmail(userName, emailAddress) {
    await sendEmail({
        to: emailAddress,
        subject: "Password Changed on Who Is It",
        bodyText: `Hello ${userName},
        \r\n\r\nYour password was just now successfully changed on Who Is It.
        \r\n\r\nhttp://whoisit.tetrayoubetra.com/users/login#forgot
        \r\n\r\nIf you did not initiate this reset, please immediately re-aquire access to your account via the Forgot Password method.
        \r\n\r\nWe recommend changing your password to something more secure!
        \r\n\r\nWho Is It?!
        \r\nhttps://whoisit.tetrayoubetra.com/`,
        bodyHtml: `Hello ${userName},
        <p>Your password was just now successfully changed on Who Is It.</p>
        <p style="text-align: center"><a href="http://whoisit.tetrayoubetra.com/users/login#forgot">http://whoisit.tetrayoubetra.com/users/login#forgot</a></p>
        <p>If you did not initiate this reset, please immediately re-aquire access to your account via the Forgot Password method. We recommend changing your password to something more secure!</p>
        <p style="text-align: center"><a href="https://whoisit.tetrayoubetra.com/"><img src="data:image/png;base64,${encodeImage('./public/img/logo.png')}" alt="Who Is It Logo">
        <br>https://whoisit.tetrayoubetra.com/</a></p>`
    });
}

module.exports.renderAccount = (req, res) => {
    res.render('users/account');
}

module.exports.updateAccount = async (req, res, next) => {
    // try {
    const { email, username, password, newpassword, confirmpassword } = req.body;

    if (email && email !== req.user.email) {
        console.log("changing email")
        let emailUser = await User.findByIdAndUpdate(req.user._id, {
            email: email,
            emailVerified: false,
            verificationToken: randomstring.generate()
        }, { new: true });
        sendVerificationEmail(emailUser.username, emailUser.email, emailUser.verificationToken);
        req.flash('success', 'Updated your email successfully.');
    }

    if (req.file) {
        console.log("uploading display pic")
        let fileUser = await User.findByIdAndUpdate(req.user._id, {
            image: { url: req.file.path, filename: req.file.filename }
        }, { new: true });
        req.flash('success', 'Updated your display picture.');
    }

    if (password && newpassword == confirmpassword) {
        console.log(`changing password`)
        let passUser = await User.findById(req.user._id);
        try {
            await passUser.changePassword(password, newpassword);
            console.log("successfully changed password")
            req.flash('success', 'Successfully changed your password.');
            sendPasswordChangeEmail(passUser.username, passUser.email);
        } catch (e) {
            console.log(e);
            req.flash('error', e.message);
        }
        await passUser.save();
    } else {
        req.flash('error', 'The passwords entered did not match.');
    }

    res.redirect('/users/account');
}

module.exports.verifyAccount = async (req, res) => {
    const { verifToken } = req.params;
    if (req.user.emailVerified == true) {
        req.flash('error', 'Your email is already verified!');
        res.redirect('/users/account');
    } else {
        if (req.user.verificationToken == verifToken) {
            await User.findByIdAndUpdate(req.user._id, { emailVerified: true, verificationToken: undefined });
            req.flash('success', 'Successfully verified your email!');
            res.redirect('/users/account');
        } else {
            req.flash('error', 'Failed to verify your email, please try again.');
            res.redirect('/users/account');
        }
    }
}

module.exports.resendVerification = async (req, res) => {
    const user = await User.findByIdAndUpdate(req.user._id, { verificationToken: randomstring.generate() }, { new: true });
    sendVerificationEmail(user.username, user.email, user.verificationToken);

    req.flash('success', 'Resent verification email.');
    res.redirect('/users/account');
}

module.exports.register = async (req, res, next) => {
    try {
        const { email, username, password } = req.body;
        const user = new User({ email, username, verificationToken: randomstring.generate() });
        const registeredUser = await User.register(user, password);
        req.login(registeredUser, err => {
            if (err) return next(err);
            sendVerificationEmail(registeredUser.username, registeredUser.email, registeredUser.verificationToken);
            req.flash('success', 'Welcome!');
            res.redirect('/');
        })
    } catch (e) {
        req.flash('error', e.message);
        res.redirect('register');
    }
}

module.exports.renderLogin = (req, res) => {
    res.render('users/login');
}

module.exports.login = (req, res) => {
    req.flash('success', 'welcome back!');
    // const redirectUrl = req.session.returnTo || '/';
    const redirectUrl = (req.session.returnToMethod == "GET") ? req.session.returnTo : '/';
    delete req.session.returnTo;
    delete req.session.returnToMethod;
    delete req.session.returnToBody;
    // res.redirect(307, redirectUrl);
    res.redirect(redirectUrl);
}

module.exports.logout = (req, res) => {
    req.logout();
    // req.session.destroy();
    req.flash('success', "Goodbye!");
    res.redirect('/');
}

module.exports.initiateForgot = async (req, res) => {
    const { email } = req.body;
    const user = await User.findOneAndUpdate({ email: email }, { resetToken: randomstring.generate(), resetTokenExp: dateAdd(new Date(), 'minute', 30) }, { new: true });
    if (user) {
        sendResetEmail(user.username, user.email, user.resetToken);
        console.log("found user and reset");
    } else {
        console.log("failed to find user and reset");
    }
    req.flash('success', "If the email was found, you will receive a password reset email.");
    res.redirect('/users/login');
}

module.exports.renderResetForm = (req, res) => {
    const { resetToken } = req.params;
    res.render('users/reset', { resetToken });
}

module.exports.submitResetForm = async (req, res) => {
    const { resetToken } = req.params;
    const { newpassword, confirmpassword } = req.body;
    const user = await User.findOne({ resetToken: resetToken });
    console.log(`setting password for ${user.username} to: ${newpassword} - ${confirmpassword}`)

    if (user && user.resetTokenExp > new Date()) {
        console.log(user);

        if (newpassword && newpassword == confirmpassword) {
            console.log(`changing password`)
            let passUser = await User.findById(req.user._id);
            try {
                await passUser.setPassword(newpassword);
                console.log("successfully changed password")
                req.flash('success', 'Successfully changed your password.');
                sendPasswordChangeEmail(passUser.username, passUser.email);
            } catch (e) {
                console.log(e);
                req.flash('error', e.message);
            }
            passUser.resetToken = undefined;
            passUser.resetTokenExp = undefined;
            await passUser.save();
        } else {
            req.flash('error', 'The passwords entered did not match.');
        }

    } else {
        req.flash('error', 'The reset token has expired, please try again.');
    }
    res.redirect('/users/login');

}