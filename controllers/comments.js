const { Board } = require('../models/board');
const { Comment } = require('../models/comment');

module.exports.createComment = async (req, res) => {
    const board = await Board.findById(req.params.id);
    const comment = new Comment(req.body.comment);
    comment.parent = req.params.id;
    comment.author = req.user._id;
    comment.submitted = new Date();
    board.comments.push(comment);
    await comment.save();
    await board.save();
    req.flash('success', 'Created new comment!');
    res.redirect(`/boards/${board._id}`);
}

module.exports.deleteComment = async (req, res) => {
    const { id, commentId } = req.params;
    await Board.findByIdAndUpdate(id, { $pull: { comments: commentId } });
    await Comment.findByIdAndDelete(commentId);
    req.flash('success', 'Successfully deleted comment')
    res.redirect(`/boards/${id}`);
}

module.exports.flagComment = async (req, res) => {
    const { id, commentId } = req.params;
    console.log("flagging board")
    const comment = await Comment.findByIdAndUpdate(commentId, { flagged: true, flaggedBy: req.user._id });
    req.flash('success', 'Reported comment')
    res.redirect(`/boards/${id}`);
}

module.exports.unflagComment = async (req, res) => {
    const { id, commentId } = req.params;
    const comment = await Comment.findByIdAndUpdate(commentId, { flagged: false, flaggedBy: null });
    req.flash('success', 'Unflagged comment')
    res.redirect('/admin');
}