const catchAsync = require('../utils/catchAsync');
const stringToBoolean = require('../utils/stringToBoolean');
const copyArray = require('../utils/copyArray');
const randomIntBetween = require('../utils/randomIntBetween');
const { Game } = require('../models/game');
const { Board } = require('../models/board');

function makeCode() {
    const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let result = "";
    for (var i = 0; i < 6; i++) {
        result += chars.charAt(Math.floor(Math.random() *
            chars.length));
    }
    return result;
}

module.exports.postNewGame = async (req, res) => {
    const { currentuser } = res.locals;
    const { private, boardid } = req.body;

    const newGame = new Game({
        players: [],
        private: stringToBoolean(private),
        joinid: makeCode().toUpperCase(), // id that player 2 can enter to join if manually joining
        theme: boardid,
        phase: "selection",  // selection, deduction, response
        turn: 0,   // tracks who's turn it is 0 for player 1, 1 for player 2, used as index of players array
        turns: [],
        created: new Date(),
    });
    let theme;
    if (boardid) {
        console.log(`Looking for board: ${boardid}`)
        theme = await Board.findByIdAndUpdate(boardid, { $inc: { timesplayed: 1 } });
        if (!theme) {
            console.log(`Couldn't find: ${boardid} - Looking for first board.`);
            theme = await Board.findOneAndUpdate({}, { $inc: { timesplayed: 1 } });
        }
    } else {
        console.log(`Couldn't find: ${boardid} - Looking for first board.`);
        theme = await Board.findOneAndUpdate({}, { $inc: { timesplayed: 1 } });
        newGame.theme = theme._id;
    }
    const gameBoard = theme.getShuffledCharacters();
    const playerCard = randomIntBetween(0, 24);
    newGame.players.push({ playerid: req.sessionID, card: playerCard, board: gameBoard });

    await newGame.save();
    req.flash("success", `Joined game: ${newGame.joinid.toUpperCase()}`);
    res.redirect('/play/client');
}



module.exports.getJoinGame = async (req, res) => {
    const { joincode } = req.query;
    const codeGame = await Game.getGameByCode(joincode);

    if (codeGame) {
        if (!codeGame.gameFull()) {
            const theme = await Board.findById(codeGame.theme);
            const gameBoard = theme.getShuffledCharacters();
            const playerCard = randomIntBetween(0, 24);
            codeGame.players.push({ playerid: req.sessionID, card: playerCard, board: gameBoard });
            codeGame.locked = true;
            codeGame.phase = "question";
            await codeGame.save();

            req.flash('success', `Joined game: ${codeGame.joinid.toUpperCase()}`);
            res.redirect('/play/client');
        } else {
            req.flash('error', "The game you've specified is full.");
            res.redirect('/');
        }
    } else {
        req.flash('error', "The code you've entered is not valid.");
        res.redirect('/');
    }
}

module.exports.getFindGame = async (req, res) => {
    const randomGame = await Game.findOpenGame();
    if (randomGame) {
        const theme = await Board.findById(randomGame.theme);
        const gameBoard = theme.getShuffledCharacters();
        const playerCard = randomIntBetween(0, 24);
        randomGame.players.push({ playerid: req.sessionID, card: playerCard, board: gameBoard });
        randomGame.locked = true;
        randomGame.phase = "question";
        await randomGame.save();

        req.flash('success', `Joined game: ${randomGame.joinid.toUpperCase()}`);
        res.redirect('/play/client');
    } else {
        req.flash('error', "Could not find any open games, try starting your own!");
        res.redirect('/');
    }
}

module.exports.getClient = async (req, res) => {
    const { existingGame } = res.locals;

    const playerIndex = existingGame.getPlayerIndex(req.sessionID);
    const player = existingGame.players[playerIndex];
    const theme = await Board.findById(existingGame.theme);
    res.render('play/client', { gamecode: existingGame.joinid, theme, player, currentNav: "play" })

}

module.exports.getClientUpdate = async (req, res) => {
    const { existingGame } = res.locals;

    const { phase, winner, wintype, turn, turns } = existingGame;
    const playerIndex = existingGame.getPlayerIndex(req.sessionID);
    const players = copyArray(existingGame.players); // make a copy of the array so we can censor the opponent's card
    if (phase != "gameover") { // if not gameover, hide the opponent's card name and index
        if (players.length > 1) {
            players[1 - playerIndex].name = "?????";
            players[1 - playerIndex].card = -1;
        } else {
            players.push({ name: "?????", card: -1, board: [] })
        }
    }
    const theme = await Board.findById(existingGame.theme);
    const isYourTurn = (playerIndex == turn);
    // const flashMessages = await existingGame.getFlash(playerIndex);
    const flashMessages = [];
    res.json({ playerIndex: playerIndex, isYourTurn: isYourTurn, gamePhase: phase, gameReady: existingGame.gameReady(), gameWinner: winner, gameWintype: wintype, gameTurn: turn, gameTurns: turns, theme, players: players, flashMessages: flashMessages })
}

module.exports.postClientQuestion = async (req, res) => {
    const { question } = req.body;
    const { existingGame } = res.locals;

    const playerIndex = existingGame.getPlayerIndex(req.sessionID);
    await existingGame.askQuestion(playerIndex, question);
    res.json({ message: "Posted question" });
}

module.exports.postClientResponse = async (req, res) => {
    const { response } = req.body;
    const { existingGame } = res.locals;

    const playerIndex = existingGame.getPlayerIndex(req.sessionID);
    await existingGame.makeResponse(playerIndex, response);
    res.json({ message: "Posted response" });
}

module.exports.postClientGuess = async (req, res) => {
    const { guess } = req.body;
    const { existingGame } = res.locals;

    const playerIndex = existingGame.getPlayerIndex(req.sessionID);
    await existingGame.makeGuess(playerIndex, guess);
    res.json({ message: "Posted guess" });
}

module.exports.postClientElimination = async (req, res) => {
    const { elimination } = req.body;
    const { existingGame } = res.locals;

    const playerIndex = existingGame.getPlayerIndex(req.sessionID);
    await existingGame.makeElimination(playerIndex, elimination);
    res.json({ message: "Posted elimination" });
}

module.exports.getEndGame = async (req, res) => {
    const { joinid } = req.params;
    const existingGame = await Game.findOneAndDelete({ "players.playerid": req.sessionID });
    if (existingGame) {
        req.flash('success', `Ended game: ${existingGame.joinid}`);
        res.redirect('/');
    } else {
        req.flash('error', "The requested game does not exist.");
        res.redirect('/');
    }
}

module.exports.getAdminEndGame = async (req, res) => {
    const { joinid } = req.params;
    const existingGame = await Game.findOneAndDelete({ joinid: joinid });
    if (existingGame) {
        console.log(`Ended game: ${existingGame.joinid}`);
        req.flash('success', `Ended game: ${existingGame.joinid}`);
        res.redirect('/play/debug');
    } else {
        req.flash('error', "The requested game does not exist.");
        res.redirect('/play/debug');
    }
}

module.exports.getDebug = async (req, res) => {
    const games = await Game.find();
    res.render('play/debug', { games: games });
}