const { Board } = require('../models/board');
const { Comment } = require('../models/comment');
const { User } = require('../models/user');

module.exports.index = async (req, res) => {
    const boards = await Board.find({ flagged: true })
        .populate({
            path: 'comments',
            populate: {
                path: 'author'
            }
        }).populate('author');

    const comments = await Comment.find({ flagged: true }).populate('author');

    res.render('admins/index', { boards, comments })
}

module.exports.banUser = async (req, res) => {
    const { userId } = req.params;
    const user = await User.findByIdAndUpdate(userId, { banned: true });
    req.flash('success', `Banned user: ${user.username}`)
    res.redirect('/admin');
}
module.exports.unbanUser = async (req, res) => {
    const { userId } = req.params;
    const user = await User.findByIdAndUpdate(userId, { banned: false });
    req.flash('success', `Unbanned user: ${user.username}`)
    res.redirect('/admin');
}