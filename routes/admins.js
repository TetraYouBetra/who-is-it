const express = require('express');
const router = express.Router({ mergeParams: true });
const { isLoggedIn, isModerator } = require('../middleware');
const { Board } = require('../models/board');
const { Comment } = require('../models/comment');
const admins = require('../controllers/admins');
const ExpressError = require('../utils/ExpressError');
const catchAsync = require('../utils/catchAsync');

router.route('/')
    .get(isLoggedIn, isModerator, catchAsync(admins.index))

router.route('/ban')
    .get(isLoggedIn, isModerator, catchAsync(admins.bannedUserIndex))

router.route('/ban/:userId')
    .patch(isLoggedIn, isModerator, catchAsync(admins.banUser))

module.exports = router;