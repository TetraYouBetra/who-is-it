const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn, usernameToLowerCase } = require('../middleware');
const { User } = require('../models/user');
const users = require('../controllers/users');
// display pici  components
const multer = require('multer');
const { storage } = require('../cloudinary');
const upload = multer({ storage });

router.route('/register')
    .post(usernameToLowerCase, catchAsync(users.register));

router.route('/account')
    .get(isLoggedIn, users.renderAccount)
    .put(isLoggedIn, upload.single('image'), catchAsync(users.updateAccount));

router.route('/verify')
    .get(isLoggedIn, catchAsync(users.resendVerification))

router.route('/verify/:verifToken')
    .get(isLoggedIn, catchAsync(users.verifyAccount))

router.route('/login')
    .get(users.renderLogin)
    .post(usernameToLowerCase, passport.authenticate('local', { failureFlash: true, failureRedirect: '/users/login' }), users.login)

router.route('/forgot')
    .post(catchAsync(users.initiateForgot))

router.route('/forgot/:resetToken')
    .get(users.renderResetForm)
    .patch(catchAsync(users.submitResetForm))

router.get('/logout', users.logout)

module.exports = router;