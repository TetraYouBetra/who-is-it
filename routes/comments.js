const express = require('express');
const router = express.Router({ mergeParams: true });
const { validateComment, isLoggedIn, isCommentAuthor, isModerator } = require('../middleware');
const { Board } = require('../models/board');
const { Comment } = require('../models/comment');
const comments = require('../controllers/comments');
const ExpressError = require('../utils/ExpressError');
const catchAsync = require('../utils/catchAsync');

router.post('/', isLoggedIn, validateComment, catchAsync(comments.createComment))

router.delete('/:commentId', isLoggedIn, isCommentAuthor, catchAsync(comments.deleteComment))

router.patch('/:commentId/flag', isLoggedIn, catchAsync(comments.flagComment));
router.patch('/:commentId/unflag', isLoggedIn, isModerator, catchAsync(comments.unflagComment));


module.exports = router;