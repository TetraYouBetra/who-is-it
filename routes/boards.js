const express = require('express');
const router = express.Router();
const boards = require('../controllers/boards');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn, isVerified, isAuthor, validateBoard, getLiked, isModerator } = require('../middleware');
const multer = require('multer');
const { storage } = require('../cloudinary');
const upload = multer({ storage });

const { Board } = require('../models/board');

router.route('/')
    .get(catchAsync(boards.index))
    .post(isLoggedIn, isVerified, upload.array('image'), validateBoard, catchAsync(boards.createBoard))


router.get('/new', isLoggedIn, isVerified, boards.renderNewForm)

router.route('/:id')
    .get(getLiked, catchAsync(boards.showBoard))
    .put(isLoggedIn, isVerified, isAuthor, upload.array('image'), validateBoard, catchAsync(boards.updateBoard))
    .delete(isLoggedIn, isVerified, isAuthor, catchAsync(boards.deleteBoard));

router.post('/:id/like', isLoggedIn, getLiked, catchAsync(boards.toggleLike))

router.get('/:id/edit', isLoggedIn, isVerified, isAuthor, catchAsync(boards.renderEditForm))

router.patch('/:id/flag', isLoggedIn, catchAsync(boards.flagBoard));
router.patch('/:id/unflag', isLoggedIn, isModerator, catchAsync(boards.unflagBoard));

module.exports = router;