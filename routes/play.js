const express = require('express');
const router = express.Router();
const catchAsync = require('../utils/catchAsync');
const { isNotInGame, isInGame, isInGameJSON } = require('../middleware');
const play = require('../controllers/play');

router.get('/debug', catchAsync(play.getDebug));

router.post('/newgame', isNotInGame, catchAsync(play.postNewGame));

//http://localhost/play/join?joincode=124124
router.get('/joingame', isNotInGame, catchAsync(play.getJoinGame));

router.get('/findgame', isNotInGame, catchAsync(play.getFindGame));

router.get('/client', isInGame, catchAsync(play.getClient));

router.get('/client/update', isInGameJSON, catchAsync(play.getClientUpdate));

router.post('/client/askQuestion', isInGameJSON, catchAsync(play.postClientQuestion));

router.post('/client/makeResponse', isInGameJSON, catchAsync(play.postClientResponse));

router.post('/client/makeGuess', isInGameJSON, catchAsync(play.postClientGuess));

router.post('/client/makeElimination', isInGameJSON, catchAsync(play.postClientElimination));


// needs to be POST
router.get('/endGame', catchAsync(play.getEndGame));

router.post('/adminEndGame/:joinid', catchAsync(play.getAdminEndGame))

module.exports = router;