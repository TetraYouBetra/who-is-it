const ExpressError = require('./utils/ExpressError');
const { boardSchema, commentSchema } = require('./schemas.js');
const { Game } = require('./models/game');
const { Board } = require('./models/board');
const { Comment } = require('./models/comment');

module.exports.usernameToLowerCase = (req, res, next) => {
    req.body.username = req.body.username.toLowerCase();
    next();
}

module.exports.isLoggedIn = (req, res, next) => {
    if (!req.isAuthenticated()) {
        req.session.returnTo = req.originalUrl
        req.session.returnToMethod = req.method;
        req.session.returnToBody = req.body;
        req.flash('error', 'You must be signed in first!');
        return res.redirect('/users/login');
    }
    next();
}

module.exports.isBanned = (req, res, next) => {
    if (req.user && req.user.banned) {
        req.logout();
        req.flash('error', 'Your account has been banned.');
        return res.redirect('/users/login');
    }
    next();
}

module.exports.isModerator = (req, res, next) => {
    if (req.user && !req.user.moderator) {
        req.flash('error', 'You are not a moderator.');
        return res.redirect('/');
    }
    next();
}

module.exports.isVerified = (req, res, next) => {
    if (req.user.emailVerified) {
        // email is verified
    } else {
        req.flash('error', 'You must verify your email first!');
        return res.redirect('/users/account');
    }
    next();
}

module.exports.isNotInGame = async (req, res, next) => {
    const existingGame = await Game.getExistingGame(req.sessionID);
    if (!existingGame) {
        res.locals.existingGame = existingGame;
        next();
    } else {
        req.flash("error", `You already have a game: ${existingGame.joinid.toUpperCase()}`);
        res.redirect('/play/client');
    }
}

module.exports.isInGame = async (req, res, next) => {
    const existingGame = await Game.getExistingGame(req.sessionID);
    if (existingGame) {
        res.locals.existingGame = existingGame;
        next();
    } else {
        req.flash("error", `You are not currently in a game.`);
        res.status(500).redirect('/');
    }
}

module.exports.isInGameJSON = async (req, res, next) => {
    const existingGame = await Game.getExistingGame(req.sessionID);
    if (existingGame) {
        res.locals.existingGame = existingGame;
        next();
    } else {
        res.status(500).json({ message: "Failed to find existing game.", gamePhase: "disconnected", gameReady: null })
    }
}


module.exports.validateBoard = (req, res, next) => {
    const { error } = boardSchema.validate(req.body);

    if (error) {
        const msg = error.details.map(el => el.message).join(',')
        throw new ExpressError(msg, 400)
    } else {
        next();
    }
}

module.exports.getLiked = async (req, res, next) => {
    const { id } = req.params;
    const board = await Board.findById(id);
    if (req.user) {
        res.locals.boardLiked = board.likes.indexOf(req.user._id);
    } else {
        res.locals.boardLiked = -1;
    }
    next();
}

module.exports.isAuthor = async (req, res, next) => {
    const { id } = req.params;
    const board = await Board.findById(id);
    if (!board.author.equals(req.user._id) && !req.user.moderator) {
        req.flash('error', 'You do not have permission to do that!');
        return res.redirect(`/boards/${id}`);
    }
    next();
}

module.exports.isCommentAuthor = async (req, res, next) => {
    const { id, commentId } = req.params;
    const comment = await Comment.findById(commentId);
    if (!comment.author.equals(req.user._id) && !req.user.moderator) {
        req.flash('error', 'You do not have permission to do that!');
        return res.redirect(`/boards/${id}`);
    }
    next();
}

module.exports.validateComment = (req, res, next) => {
    const { error } = commentSchema.validate(req.body);
    if (error) {
        const msg = error.details.map(el => el.message).join(',')
        throw new ExpressError(msg, 400)
    } else {
        next();
    }
}

