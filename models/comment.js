const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timeSince = require('../utils/timeSince');

const commentSchema = new Schema({
    parent: String,
    body: String,
    submitted: Date,
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    flagged: {
        type: Boolean,
        default: false
    }
});

commentSchema.virtual('timeSinceCreation').get(function () {
    return timeSince(this.submitted);
});

const Comment = mongoose.model('Comment', commentSchema);

module.exports = { Comment, commentSchema };