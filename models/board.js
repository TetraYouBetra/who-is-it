const mongoose = require('mongoose');
const { Comment } = require('./comment');
const Schema = mongoose.Schema;
const timeSince = require('../utils/timeSince');
const shuffleArray = require('../utils/shuffleArray');

const ImageSchema = new Schema({
    url: String,
    filename: String,
    name: String
});

ImageSchema.virtual('thumbnail').get(function () {
    return this.url.replace('/upload', '/upload/w_200');
});

const opts = { toJSON: { virtuals: true } };

const boardSchema = new Schema({
    title: String,
    images: [ImageSchema],
    description: String,
    likes: [String],
    numlikes: Number,
    submitted: Date,
    timesplayed: {
        type: Number,
        default: 0
    },
    published: {
        type: Boolean,
        default: false
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    comments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Comment'
        }
    ],
    flagged: {
        type: Boolean,
        default: false
    },
    flaggedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
}, opts);

boardSchema.virtual('timeSinceCreation').get(function () {
    return timeSince(this.submitted);
});

boardSchema.methods.getUserLikeIndex = function (userID) {
    return this.likes.indexOf(userID);
};

boardSchema.post('findOneAndDelete', async function (doc) {
    if (doc) {
        await Comment.deleteMany({
            _id: {
                $in: doc.comments
            }
        });
    }
})

boardSchema.methods.getShuffledCharacters = function () {
    console.log("shuffling")
    return shuffleArray(this.images);
};

const Board = mongoose.model('Board', boardSchema);

module.exports = { Board, boardSchema };