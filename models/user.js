const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');

const ImageSchema = new Schema({
    url: String,
    filename: String,
});

ImageSchema.virtual('thumbnail').get(function () {
    return this.url.replace('/upload', '/upload/w_200');
});

const UserSchema = new Schema({
    // username and password managed by passport module
    email: {
        type: String,
        required: true,
        unique: true,
    },
    image: ImageSchema,
    emailVerified: {
        type: Boolean,
        default: false
    },
    verificationToken: String,
    resetToken: String,
    resetTokenExp: Date,
    moderator: {
        type: Boolean,
        default: false
    },
    banned: {
        type: Boolean,
        default: false
    }
});

UserSchema.plugin(passportLocalMongoose);

const User = mongoose.model('User', UserSchema);

module.exports = { User, UserSchema };