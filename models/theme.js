const mongoose = require('mongoose');
const shuffleArray = require('../utils/shuffleArray');

const themeSchema = new mongoose.Schema({
    name: String,
    image: String,
    created: Date,
    creator: String,
    likes: Number,
    characters: [{ name: String, image: String }]
});

themeSchema.virtual("created_lcl").get(function () {
    return this.created.toLocaleDateString(undefined, {
        year: 'numeric',
        month: 'numeric', day: 'numeric'
    });
});

themeSchema.methods.getShuffledCharacters = function () {
    return shuffleArray(this.characters);
};

const Theme = mongoose.model('theme', themeSchema);

module.exports = { Theme, themeSchema };