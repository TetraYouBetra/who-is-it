const mongoose = require('mongoose');
const copyArray = require('../utils/copyArray');
const stringToBoolean = require('../utils/stringToBoolean');

const gameSchema = new mongoose.Schema({
    players: [{ playerid: String, card: Number, board: [{ name: String, url: String, eliminated: Boolean }], flash: [{ name: String, data: {} }] }], //contains the id of the player's session and their card id
    locked: Boolean, // game starts out false and goes true when all players join
    private: Boolean, // determines weather a random can join or if join by code only
    theme: String,  // id of the theme in use on this game
    joinid: String, // id that player 2 can enter to join if manually joining
    phase: String,  // selection, question, response
    winner: Number, // id of player that's one - game will go on until a winner
    wintype: String, // successfulguess or failedguess
    created: Date,
    turn: Number,   // tracks who's turn it is 0 for player 1, 1 for player 2, used as index of players array
    turns: [{ player: Number, messagetype: String, message: String, response: Boolean }],  // player is player index, type is "response" or "question"
    expireAt: {
        type: Date,
        default: Date.now,
        index: { expires: '60m' },
    },
});

// hard stops any attempts to add a 3rd player
gameSchema.pre('validate', function (next) {
    if (this.players.length > 2) throw ("This game is full.");
    next();
});


// DOCUMENT METHODS

// true if both players are present
gameSchema.methods.gameFull = function (playerIndex, cardname) {
    return (this.players.length > 1);
};

// true if both players have selected their card
gameSchema.methods.gameReady = function (playerIndex, cardname) {
    if (this.players.length > 1) {
        if (this.players[0].card && this.players[1].card) {
            return true
        }
        return false
    }
    return false
};

// return player index of supplied session id
gameSchema.methods.getPlayerIndex = function (sessionID) {
    for (let i = 0; i < this.players.length; i++) {
        if (this.players[i].playerid == sessionID) {
            return i;
        }
    }
    return -1;
};

// start game: phase in Selection. both players select a card. 
// deduction phase: first player asks a deduction or makes a guess
// response phase: second player is prompted for yes or no
// repeat for second player

// gameSchema.methods.addFlash = async function (playerIndex, flashName, flashData) {
//     console.log(`adding flash, ${playerIndex}`);
//     console.log(playerIndex, flashName, flashData)
//     this.players[playerIndex].flash.push({
//         name: flashName,
//         data: flashData
//     });
//     await this.save();
// }
// gameSchema.methods.getFlash = async function (playerIndex) {
//     console.log(`getting flash, ${playerIndex}`);
//     if (this.players[playerIndex].flash.length > 0) {
//         console.log(this.players[playerIndex].flash);
//         const newArray = copyArray(this.players[playerIndex].flash);
//         this.players[playerIndex].flash = [];
//         await this.save();
//         return newArray;
//         //return [];
//     } else {
//         return [];
//     }
// }

// both players make this call from their client which records their secret character
// once it detects that both players have selected a card, game proceeds to deduction phase
gameSchema.methods.selectSecretCard = async function (playerIndex, cardname) {
    if (this.phase == "selection") {
        this.players[playerIndex].card = cardname;
        if (this.gameFull()) {
            if (this.players[0].card && this.players[1].card) {
                this.phase = "question";
                this.turn = 0;
            }
        }
        await this.save();
    }
}

// when game is in response phase, allow player to make a deduction
// this is pushed to the turns list and the phase changes to response
// turn does not change until a response is received
gameSchema.methods.askQuestion = async function (playerIndex, message) {
    if (playerIndex == this.turn && this.phase == "question") {
        this.turns.push({
            player: playerIndex,
            messagetype: "question",
            message: message
        });
        this.phase = "response";
        await this.save();
        //await this.addFlash(playerIndex, "newquestion", {});
    }
}


// opponent makes a yes or no response to the previous deduction
// this is saved to the turns array
// game goes to deduction mode and the turn switches to the opponent
// response mode always apply to the opponent of the current turn
// deduction made from player 0 waits for a response from player 1 and vice versa
gameSchema.methods.makeResponse = async function (playerIndex, message) {
    if (playerIndex == 1 - this.turn && this.phase == "response") {
        this.turns[this.turns.length - 1].response = stringToBoolean(message);
        this.phase = "question";
        this.turn = 1 - this.turn;
        await this.save();
        //await this.addFlash(1 - playerIndex, "newanswer", { message: message });
    }
}

gameSchema.methods.makeElimination = async function (playerIndex, elimination) {
    if (this.phase == "response" || this.phase == "question") {
        let curVal = (this.players[playerIndex].board[elimination].eliminated === true) ? true : false;
        this.players[playerIndex].board[elimination].eliminated = 1 - curVal;
        await this.save();
    }
}

// once ready, the player can make a guess
// if the guess is correct, player wins, otherwise the opponent wins
gameSchema.methods.makeGuess = async function (playerIndex, guessIndex) {
    if (playerIndex == this.turn && this.phase == "question") {
        const guessName = this.players[playerIndex].board[guessIndex].name;
        const opponentCardIndex = this.players[1 - playerIndex].card;
        const opponentCardName = this.players[1 - playerIndex].board[opponentCardIndex].name;

        this.turns.push({
            player: playerIndex,
            messagetype: "question",
            message: `Is your character ${guessName}?`
        });

        const guessResponse = (guessName == opponentCardName) ? true : false;
        this.turns[this.turns.length - 1].response = guessResponse;

        if (guessResponse) {
            this.winner = playerIndex;
            this.wintype = "successfulguess";
        } else {
            this.winner = 1 - playerIndex;
            this.wintype = "failedguess";
        }
        this.phase = "gameover"
        await this.save();
    }
}

gameSchema.virtual("created_lcl").get(function () {
    return this.created.toLocaleDateString(undefined, {
        year: 'numeric',
        month: 'numeric', day: 'numeric'
    });
});

// STATIC METHODS
// returns game if the player is already in a game
gameSchema.statics.getExistingGame = async function (sessionID) {
    const result = await this.findOne({ "players.playerid": sessionID });
    return result || false;
};
// returns a game with the supplied join code
gameSchema.statics.getGameByCode = async function (joincode) {
    const result = await this.findOne({ joinid: joincode.toUpperCase() });
    return result || false;
};
// returns a non private game with a free slot
gameSchema.statics.findOpenGame = async function () {
    const result = await this.findOne({ private: false, "players": { $size: 1 } });
    return result;
}

const Game = mongoose.model('game', gameSchema);

module.exports = { Game, gameSchema };