//const sessionplayerid = "6147ea01cacdbf18953b47e9"; this is defined and passed through from client.ejs
let charactertiles = document.querySelectorAll('.charactertile');
let turnstatus = document.querySelector('#turnstatus');

let playerPortrait = document.querySelector('#cont-player');
let playerCardName = document.querySelector('#player-card-name');
let opponentPortrait = document.querySelector('#cont-opponent');
let opponentCardName = document.querySelector('#opponent-card-name');
//board cards under id: board-card-name
let boardCards = document.querySelectorAll('.board-card');
let curQstnText = document.querySelector('#cur-qstn-text');

let guessMode = false;
let gameOver = false;
let turnHistory = 16; // how many messages to show in log from most recent
let sessionData = {};
let playerTurns = [];
let playerTurnsOld = [];
let updateTurnViewer = false;
let playerTurnView = 0;

for (let i = 0; i < 24; i++) {
    let boardCard = boardCards[i];
    boardCard.addEventListener("click", async (e) => {
        let boardCardValue = boardCard.getAttribute('data-value');
        if (!guessMode) {
            // let boardCardX = document.querySelector(`#xmark${i}`)
            // boardCardX.classList.toggle("d-none");
            await makeElimination(boardCardValue);
        } else {
            await makeGuess(boardCardValue);
        }
    });
}

// lobby buttons
let btnGamecodeCopy = document.querySelector('#btn-gamecode-copy');
btnGamecodeCopy.addEventListener("click", async (e) => {
    let gameCode = document.querySelector('#gamecodetext');
    copyToClipboard(`${location.protocol}//${url_domain()}/play/joingame?joincode=${gameCode.innerText.trim().slice(0, 6)}`);
    btnGamecodeCopy.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-clipboard-check" viewBox="0 0 16 16">
    <path fill-rule="evenodd" d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
    <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
    <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
  </svg > `;
});

let btnEndgame = document.querySelectorAll('.btn-endgame')
for (let btn of btnEndgame) {
    btn.addEventListener('click', async (e) => {
        location.href = '/play/endGame';
    });
}

let btnOptions = document.querySelector('#btn-options')
btnOptions.addEventListener("click", async (e) => {
    showPopup("#lobby", true);
});

// bottom controls buttons
function responseToFriendly(string) {
    if (string) {
        switch (string.toLowerCase().trim()) {
            case true: case "on": case "true": case "yes": case "1": return "&check;";
            case false: case "off": case "false": case "no": case "0": case null: return "&#x2717;";

            default: return Boolean(string);
        }
    } else {
        return "?";
    }
}

function updateTurnView() {
    if (playerTurns.length > 0) {
        const questionList = document.querySelector('#playerquestionlist-text');
        questionList.innerText = "";
        let newList = "";
        // itterates through the playerTurns array
        // for each turn build a turn message
        for (let i = 0; i < playerTurns.length; i++) {
            turn = playerTurns[i];
            let turnMessage = "";
            let turnListItem = "";
            let turnListItemAnswer = "";
            if (turn.response !== undefined) {
                turnMessage = `${turn.message} - ${responseToFriendly(turn.response.toString())}`;
                turnListItem = `${turn.message}`;
                turnListItemAnswer = `${responseToFriendly(turn.response.toString())}`;
            } else {
                turnMessage = `${turn.message} - Waiting...`;
                turnListItem = `${turn.message}`;
                turnListItemAnswer = `?`;
            }
            if (i == playerTurnView) {
                curQstnText.innerHTML = turnMessage;
            }
            // if on the last turn set the html display inner html
            // if (i == playerTurns.length - 1) {
            //     curQstnText.innerHTML = turnMessage;
            // }
            newList += `<tr><td class="question-list-item-answer">Q${i + 1}</td><td class="question-list-item">${turnListItem}</td><td class="question-list-item-answer">${turnListItemAnswer}</td></tr>`;
        }
        // update questions popup
        questionList.innerHTML = newList;

        // update the bottom turn viewer
        if (updateTurnViewer) {
            playerTurnView = playerTurns.length - 1;
        }
    } else {
        curQstnText.innerText = "...";
    }
}
let btnBack = document.querySelector('#btn-back');
btnBack.addEventListener("click", async (e) => {
    if (playerTurnView > 0) {
        playerTurnView--;
    }
    updateTurnView();
});

let btnNext = document.querySelector('#btn-next');
btnNext.addEventListener("click", async (e) => {
    if (playerTurnView < playerTurns.length - 1) {
        playerTurnView++;
    }
    updateTurnView();
});

let btnAction = document.querySelector('#btn-action');
btnAction.addEventListener("click", async (e) => {
    disableGuessMode();
    showPopup("#playerprompt", true);
});


// your turn choice buttons
function enableGuessMode() {
    guessMode = true;
    for (let i = 0; i < 24; i++) {
        let boardCard = boardCards[i];
        boardCard.classList.add('board-card-selectmode');
    }
}
function disableGuessMode() {
    guessMode = false;
    for (let i = 0; i < 24; i++) {
        let boardCard = boardCards[i];
        boardCard.classList.remove('board-card-selectmode');
    }
}

let btnGuess = document.querySelector('#btn-guess');
btnGuess.addEventListener("click", async (e) => {
    enableGuessMode();
    hidePopup();
});

let btnQuestion = document.querySelector('#btn-question');
btnQuestion.addEventListener("click", async (e) => {
    showPopup("#playerquestion", true)
});

let inputPlayerQuestion = document.querySelector('#input-playerquestion');
let btnSend = document.querySelector('#btn-send');
btnSend.addEventListener("click", async (e) => {
    if (inputPlayerQuestion.value) {
        askQuestion(inputPlayerQuestion.value).then(res => {
            hidePopup();
            inputPlayerQuestion.value = "";
            updateSessionData().then(res => {
                updateDisplay(res);
            })
        });
    }
});

// responding  buttons
let btnResponseYes = document.querySelector('#btn-response-yes');
btnResponseYes.addEventListener("click", async (e) => {
    await makeResponse("yes").then(res => {
        hidePopup();
        updateSessionData().then(res => {
            updateDisplay(res);
        })
    });
});

let btnResponseNo = document.querySelector('#btn-response-no');
btnResponseNo.addEventListener("click", async (e) => {
    await makeResponse("no").then(res => {
        hidePopup();
        updateSessionData().then(res => {
            updateDisplay(res);
        })
    });
});

let currentQuestionBox = document.querySelector('#cur-qstn-box');
currentQuestionBox.addEventListener("click", async (e) => {
    if (playerTurns.length > 0) {
        showPopup("#playerquestionlist", true);
    } else {

    }
});

let closePopup = document.querySelector('#cont-btn-closepop');
closePopup.addEventListener("click", async (e) => {
    hidePopup();
});


// let btnEndgame = document.querySelector('#btnEndgame');
// btnEndgame.addEventListener("click", async (e) => {
//     location.href = '/play/endGame';
// });
function copyToClipboard(text) {
    var dummy = document.createElement("textarea");
    // to avoid breaking orgain page when copying more words
    // cant copy when adding below this code
    // dummy.style.display = 'none'
    document.body.appendChild(dummy);
    //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}

function hideLobby() {
    if (closePopup.classList.contains("d-none")) {
        let lobbyPopup = document.querySelector('#lobby');
        if (!lobbyPopup.classList.contains('d-none')) {
            hidePopup();
        }
    }
}

function hidePopup() {
    // hide all messages
    let cover = document.querySelector('#cover');
    cover.classList.add("d-none");

    let messages = document.querySelectorAll(".message");
    for (let message of messages) {
        message.classList.add("d-none");
    }
}

function showPopup(popupElementID, canClose) {
    hidePopup();
    let popup = document.querySelector(popupElementID);
    popup.classList.remove("d-none");

    if (canClose) {
        closePopup.classList.remove("d-none");
    } else {
        closePopup.classList.add("d-none");
    }

    let cover = document.querySelector('#cover');
    cover.classList.remove("d-none");

    if (popupElementID == "#playerquestion") {
        inputPlayerQuestion.focus();
    }
}

///////////////////////////////
function copyArray(arrayIn) {
    return JSON.parse(JSON.stringify(arrayIn));
}

// ("get",{ params: {}, headers: {} },"/play/client/update", onSuccess(res))
async function sendAxiosRequest(method, config, path, onSuccess) {
    try {
        const res = await axios[method](`${location.protocol}//${url_domain()}${path}`, config);
        if (res.status == 200) {
            onSuccess(res)
        } else {
            throw Error(res.status);
        }
        return res;
    } catch (e) {
        console.log(e)
        console.log("Failed to connect to game session.");
        return { status: 500 };
    }
}

function updateSessionData() {
    return sendAxiosRequest("get", { params: {}, headers: {} }, "/play/client/update", (res) => {
        console.log(res);

        playerTurns = res.data.gameTurns.filter(function (e) {
            return e.player == res.data.playerIndex;
        });

        if (playerTurnsOld.length != playerTurns.length) {
            updateTurnViewer = true;
            playerTurnsOld = copyArray(playerTurns);
        } else {
            updateTurnViewer = false;
        }
        sessionData = res.data;
    });
}

function makeResponse(response) {
    return sendAxiosRequest("post", { response: response, params: {}, headers: {} }, "/play/client/makeResponse", (res) => {
        if (res.data.gamePhase !== "ended") {
            updateSessionData().then(res => {
                updateDisplay(res);
            });
        }
    });
}

function makeElimination(elimination) {
    return sendAxiosRequest("post", { elimination: elimination, params: {}, headers: {} }, "/play/client/makeElimination", (res) => {
        if (res.data.gamePhase !== "ended") {
            updateSessionData().then(res => {
                updateDisplay(res);
            });
        }
    });
}

function askQuestion(question) {
    return sendAxiosRequest("post", { question: question, params: {}, headers: {} }, "/play/client/askQuestion", (res) => {
        if (res.data.gamePhase !== "ended") {
            updateSessionData().then(res => {
                updateDisplay(res);
            });
        }
    });
}

function makeGuess(guess) {
    return sendAxiosRequest("post", { guess: guess, params: {}, headers: {} }, "/play/client/makeGuess", (res) => {
        if (res.data.gamePhase !== "ended") {
            updateSessionData().then(res => {
                updateDisplay(res);
            });
        }
    });
}

function showGameOver(header, message) {
    let endheader = document.querySelector("#endheader");
    let endmessage = document.querySelector("#endmessage");
    endheader.innerText = header;
    endmessage.innerText = message;
    showPopup("#gameover");
}

function url_domain(data) {
    var a = document.createElement('a');
    a.href = data;
    return a.hostname;
}

function updateDisplay(res) {
    if (res.status == 200) {
        const { gameWinner, gameWintype, playerIndex, gameTurn, gameTurns, gamePhase, gameReady, isYourTurn, players } = res.data;
        const player = players[playerIndex];
        const opponent = players[1 - playerIndex];

        btnAction.classList.remove("center-bounce");
        btnAction.classList.add("center-element-button");
        btnAction.disabled = true;

        let imgPortraitPlayer = document.querySelector('#img-portrait-player');
        imgPortraitPlayer.src = player.board[player.card].url;
        let playerCardName = document.querySelector('#player-card-name');
        playerCardName.innerText = player.board[player.card].name;

        let imgPortraitOpponent = document.querySelector('#img-portrait-opponent');
        if (opponent.card >= 0) {
            imgPortraitOpponent.src = opponent.board[opponent.card].url;
            let opponentCardName = document.querySelector('#opponent-card-name');
            opponentCardName.innerText = opponent.board[opponent.card].name;
        } else {
            imgPortraitOpponent.src = "/img/emptyportrait.jpg";
            opponentCardName.innerText = "?????";
        }

        // player board
        console.log(player.board)
        for (let i = 0; i < player.board.length; i++) {
            let cardImg = document.querySelector(`#board-card${i}`);
            let cardName = document.querySelector(`#board-card-name${i}`);
            let cardXmark = document.querySelector(`#board-xmark${i}`);
            cardImg.src = player.board[i].url;
            cardName.innerText = player.board[i].name;
            if (player.board[i].eliminated) {
                cardXmark.classList.remove('d-none');
            } else {
                cardXmark.classList.add('d-none');
            }
        }
        // opponent board
        for (let i = 0; i < opponent.board.length; i++) {
            let cardXmark = document.querySelector(`#opponent-board-xmark${i}`);
            if (opponent.board[i].eliminated) {
                cardXmark.classList.remove('d-none');
            } else {
                cardXmark.classList.add('d-none');
            }
        }

        updateTurnView();


        // turn indicator
        if (isYourTurn) {
            let contPlayer = document.querySelector('#cont-player');
            contPlayer.classList.remove('unlit-portrait');
            contPlayer.classList.add('lit-portrait');

            let contOpponent = document.querySelector('#cont-opponent');
            contOpponent.classList.add('unlit-portrait');
            contOpponent.classList.remove('lit-portrait');
        } else {
            let contPlayer = document.querySelector('#cont-player');
            contPlayer.classList.remove('lit-portrait');
            contPlayer.classList.add('unlit-portrait');

            let contOpponent = document.querySelector('#cont-opponent');
            contOpponent.classList.add('lit-portrait');
            contOpponent.classList.remove('unlit-portrait');
        }

        if (res.data.gamePhase == "selection") {
            if (!res.data.gameReady) {
                showPopup("#lobby");
            }
            // skip selection
        } else if (res.data.gamePhase == "question") {
            hideLobby();
            if (res.data.isYourTurn == true) {
                btnAction.classList.add("center-bounce");
                btnAction.classList.remove("center-element-button");
                btnAction.disabled = false;
                // turnstatus.innerText = "Its your turn!"
                // turninstructions.innerText = "Ask a question or guess your oponent's character!"
                // questioncontainer.classList.remove("d-none");
            } else {
                // turnstatus.innerText = "Its your opponent's turn!"
                // turninstructions.innerText = "They will ask a yes or no question about your character or make a deduction."
            }
        } else if (res.data.gamePhase == "response") {
            if (res.data.isYourTurn == true) {
                // turnstatus.innerText = "Its your turn!"
                // turninstructions.innerText = "Waiting on your opponent to answer your question."
            } else {
                // turnstatus.innerText = "Its your opponent's turn!"
                // turninstructions.innerText = "Please answer your opponent's question."
                // responsecontainer.classList.remove("d-none");
                // responsecontainer.classList.add("d-flex");
                const questionText = document.querySelector('#questiontext');
                questionText.innerText = res.data.gameTurns[res.data.gameTurns.length - 1].message;
                showPopup("#opponentquestion");
            }
        } else if (res.data.gamePhase == "gameover") { // winner/gameover
            gameOver = true;
            if (res.data.gameWinner == res.data.playerIndex) { // for you won
                let winmessage = "You guessed your opponent's card!";
                if (res.data.gameWintype == "failedguess") {
                    winmessage = "Your opponent guessed the wrong card, you win by default!";
                }
                showGameOver("You win!", winmessage);
            } else { // for opponent won
                let winmessage = "Your opponent guessed your card!";
                if (res.data.gameWintype == "failedguess") {
                    winmessage = "You guessed the wrong card, you lose by default!";
                }
                showGameOver("You lose!", winmessage);
            }
        } else { // anything else like game ended or disconnected
            gameOver = true;
            showGameOver("Game ended", "The game was ended or no longer exists.");
        }
        // clear and draw chat bubbles
        // turnlog.innerText = "";
        //res.data.gameTurns.forEach(function (turn, i) {
        if (res.data.gameTurns) {
            for (let i = res.data.gameTurns.length - 1; i >= res.data.gameTurns.length - turnHistory && i >= 0; i--) {
                // let turn = res.data.gameTurns[i];
                // let newTurnBubble = document.createElement("div");
                // //newTurnBubble.innerText = `Player: ${turn.player}, Type: ${turn.messagetype}, Message: ${turn.message}`;
                // newTurnBubble.innerText = `${turn.message}`;
                // let thisClass = (turn.player == res.data.playerIndex) ? 'playermsg' : 'opponentmsg';
                // newTurnBubble.classList.add(thisClass);
                // newTurnBubble.classList.add("chatbubble");
                // if (turn.messagetype == "question" || i == res.data.gameTurns.length - 1) {
                //     newTurnBubble.classList.add("last");
                // };
                // turnlog.prepend(newTurnBubble);
                //turnlog.append(document.createElement("br"));
            }
        }
    } else { // anything else like game ended or disconnected
        gameOver = true;
        showGameOver("Game ended", "The game was ended or no longer exists.");
    }
}


// initial data load and game refresh loop
// refreshes every 5 seconds - a psuedo connection
updateSessionData().then(res => {
    updateDisplay(res);
});
const interval = setInterval(function () {
    if (!gameOver) {
        updateSessionData().then(res => {
            updateDisplay(res);
        })
    }
}, 5000);