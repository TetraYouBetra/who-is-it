//const sessionplayerid = "6147ea01cacdbf18953b47e9"; this is defined and passed through from client.ejs
let charactertiles = document.querySelectorAll('.charactertile');
let turnstatus = document.querySelector('#turnstatus');
let turninstructions = document.querySelector('#turninstructions');
let turnlog = document.querySelector('#turnlog');
let choicebuttons = document.querySelectorAll('.choice');
let inputQuestion = document.querySelector("#inputQuestion");
let questioncontainer = document.querySelector('.questionbuttons');
let responsecontainer = document.querySelector('.responsebuttons');
let guessMode = false;
let gameOver = false;
let turnHistory = 16; // how many messages to show in log from most recent

for (let tile of charactertiles) {
    tile.addEventListener("click", async (e) => {
        if (!guessMode) {
            tile.classList.toggle("hidden");
        } else {
            await makeGuess(tile.getAttribute('data-value'));
        }
    });
}

let btnGuess = document.querySelector('#btnGuess');
btnGuess.addEventListener("click", async (e) => {
    guessMode = 1 - guessMode;
    btnGuess.classList.toggle("disabled");
});

let responsebuttons = document.querySelectorAll('.response');
for (let response of responsebuttons) {
    response.addEventListener("click", async (e) => {
        await makeResponse(response.innerText);
    });
}

let btnQuestion = document.querySelector('#btnQuestion');
btnQuestion.addEventListener("click", async (e) => {
    await askQuestion(inputQuestion.value);
    inputQuestion.value = "";
});

let btnEndgame = document.querySelector('#btnEndgame');
btnEndgame.addEventListener("click", async (e) => {
    location.href = '/play/endGame';
});



///////////////////////////////
async function updateSessionData() {
    try {
        const config = { params: {}, headers: {} };
        const res = await axios.get(`http://${url_domain()}/play/client/update`, config);
        if (res.status == 200) {
            //serverdata = res.data;
            console.log(res);
            console.log(`Game ready: ${res.data.gameReady}`);
            console.log(`Game phase: ${res.data.gamePhase}`);
            console.log(`Game winner: ${res.data.gameWinner}`);
            console.log(`Game Wintype: ${res.data.gameWintype}`);
            console.log(`Game turn: ${res.data.gameTurn}`);
            console.log(`Game turns: ${res.data.gameTurns}`);
            console.log(`Is Your Turn: ${res.data.isYourTurn}`);
            if (res.data.gameReady == true) {
                //location.href = '/play/client';
            }
        } else {
            throw Error(res.status);
        }
        return res;
    } catch (e) {
        console.log(e)
        console.log("Failed to connect to game session.");
        location.href = '/';
        return { status: 500 };
        //console.log(res)
    }
};

async function makeResponse(response) {
    try {
        const config = { response: response, params: {}, headers: {} };
        const res = await axios.post(`http://${url_domain()}/play/client/makeResponse`, config);
        if (res.status == 200 && res.data.gamePhase !== "ended") {
            //serverdata = res.data;
            updateSessionData().then(res => {
                updateDisplay(res);
            })
        } else {
            throw Error(res.status);
        }
        return res;
    } catch (e) {
        console.log(e)
        console.log("Failed to connect to game session.");
        location.href = '/';
        return { status: 500 };
        //console.log(res)
    }
};

async function askQuestion(question) {
    try {
        const config = { question: question, params: {}, headers: {} };
        const res = await axios.post(`http://${url_domain()}/play/client/askQuestion`, config);
        if (res.status == 200 && res.data.gamePhase !== "ended") {
            //serverdata = res.data;
            updateSessionData().then(res => {
                updateDisplay(res);
            })
        } else {
            throw Error(res.status);
        }
        return res;
    } catch (e) {
        console.log(e)
        console.log("Failed to connect to game session.");
        location.href = '/';
        return { status: 500 };
        //console.log(res)
    }
};

async function makeGuess(guess) {
    console.log(`Guess present: ${guess}`)
    try {
        const config = { guess: guess, params: {}, headers: {} };
        const res = await axios.post(`http://${url_domain()}/play/client/makeGuess`, config);
        console.log(`Debug response: ${res}`);
        if (res.status == 200 && res.data.gamePhase !== "ended") {
            //serverdata = res.data;
            updateSessionData().then(res => {
                updateDisplay(res);
            })
            console.log(res);
        } else {
            throw Error(res.status);
        }
        return res;
    } catch (e) {
        console.log(e)
        console.log("Failed to connect to game session.");
        location.href = '/';
        return { status: 500 };
        //console.log(res)
    }
};

function updateDisplayGameOver(header, message) {
    let endscreen = document.querySelector("#endscreen");
    let gamescreen = document.querySelector("#gamescreen");
    let endheader = document.querySelector("#endheader");
    let endmessage = document.querySelector("#endmessage");
    endheader.innerText = header;
    endmessage.innerText = message;

    endscreen.classList.remove("d-none");
    gamescreen.classList.add("d-none");
}

// initial data load and screen pan
// updatetSessionData().then(res => {
// })

const interval = setInterval(function () {
    if (!gameOver) {
        updateSessionData().then(res => {
            updateDisplay(res);
        })
    }
}, 5000);

function url_domain(data) {
    var a = document.createElement('a');
    a.href = data;
    return a.hostname;
}

function updateDisplay(res) {
    responsecontainer.classList.add("d-none");
    questioncontainer.classList.add("d-none");
    responsecontainer.classList.remove("d-flex");

    if (res.status == 200) {
        if (res.data.gamePhase == "question") {
            if (res.data.isYourTurn == true) {
                turnstatus.innerText = "Its your turn!"
                turninstructions.innerText = "Ask a question or guess your oponent's character!"
                questioncontainer.classList.remove("d-none");
            } else {
                turnstatus.innerText = "Its your opponent's turn!"
                turninstructions.innerText = "They will ask a yes or no question about your character or make a deduction."
            }
        } else if (res.data.gamePhase == "response") {
            if (res.data.isYourTurn == true) {
                turnstatus.innerText = "Its your turn!"
                turninstructions.innerText = "Waiting on your opponent to answer your question."
            } else {
                turnstatus.innerText = "Its your opponent's turn!"
                turninstructions.innerText = "Please answer your opponent's question."
                responsecontainer.classList.remove("d-none");
                responsecontainer.classList.add("d-flex");
            }
        } else if (res.data.gamePhase == "gameover") { // winner/gameover
            gameOver = true;
            console.log(`game winner index: ${res.data.gameWinner}`)
            console.log(`game winner type: ${res.data.gameWintype}`)
            console.log(`your player index: ${res.data.playerIndex}`)
            if (res.data.gameWinner == res.data.playerIndex) { // for you won
                let winmessage = "You guessed your opponent's card!";
                if (res.data.gameWintype == "failedguess") {
                    winmessage = "Your opponent guessed the wrong card, you win by default!";
                }
                updateDisplayGameOver("You win!", winmessage);
            } else { // for opponent won
                let winmessage = "Your opponent guessed your card!";
                if (res.data.gameWintype == "failedguess") {
                    winmessage = "You guessed the wrong card, you lose by default!";
                }
                updateDisplayGameOver("You lose!", winmessage);
            }
        } else { // anything else like game ended or disconnected
            gameOver = true;
            updateDisplayGameOver("Game ended", "The game was ended or no longer exists.");
        }
        // clear and draw chat bubbles
        turnlog.innerText = "";
        //res.data.gameTurns.forEach(function (turn, i) {
        for (let i = res.data.gameTurns.length - 1; i >= res.data.gameTurns.length - turnHistory && i >= 0; i--) {
            let turn = res.data.gameTurns[i];
            let newTurnBubble = document.createElement("div");
            //newTurnBubble.innerText = `Player: ${turn.player}, Type: ${turn.messagetype}, Message: ${turn.message}`;
            newTurnBubble.innerText = `${turn.message}`;
            let thisClass = (turn.player == res.data.playerIndex) ? 'playermsg' : 'opponentmsg';
            newTurnBubble.classList.add(thisClass);
            newTurnBubble.classList.add("chatbubble");
            if (turn.messagetype == "question" || i == res.data.gameTurns.length - 1) {
                newTurnBubble.classList.add("last");
            };
            turnlog.prepend(newTurnBubble);
            //turnlog.append(document.createElement("br"));
        }
    }
}

updateSessionData().then(res => {
    updateDisplay(res);
});