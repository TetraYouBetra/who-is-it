if (process.env.NODE_ENV !== "production") {
    require('dotenv').config();
}

const express = require('express');
const mongoose = require('mongoose');
const ejsMate = require('ejs-mate'); // allows layout templates in EJS
const session = require('express-session');
const flash = require('connect-flash');
const ExpressError = require('./utils/ExpressError');
const catchAsync = require('./utils/catchAsync');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const { User } = require('./models/user');
const { Game } = require('./models/game');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');

const playRoutes = require('./routes/play');
const boardsRoutes = require('./routes/boards');
const commentsRoutes = require('./routes/comments');
const usersRoutes = require('./routes/users');
const adminsRoutes = require('./routes/admins');
const { isBanned } = require('./middleware');

const methodOverride = require("method-override");
const cookieParser = require('cookie-parser');
const app = express();

//DB Connection
const MongoDBStore = require("connect-mongo")(session);
const dbUrl = process.env.DB_URL || 'mongodb://localhost:27017/whoisit';

mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("DB connection opened.")
}).catch(e => {
    console.log("DB connection error: ", e);
});
const db = mongoose.connection;

// middleware
app.use(methodOverride('_method'))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser(process.env.SECRET));
app.use(flash());

const path = require('path');
app.use(express.static(path.join(__dirname, 'public'))); //initialize static resources directory
app.use(mongoSanitize({
    replaceWith: '_'
}))

app.use((req, res, next) => {
    req.requestTime = new Date();
    next();
})

const secret = process.env.SECRET || 'E9!hY4bXRSkf-A3J!!';
const store = new MongoDBStore({
    url: dbUrl,
    secret,
    touchAfter: 24 * 60 * 60
});

store.on("error", function (e) {
    console.log("SESSION STORE ERROR", e)
})

const sessionConfig = {
    store,
    name: 'session',
    secret,
    resave: false,
    saveUninitialized: true,
    cookie: {
        httpOnly: true,
        // secure: true,
        expires: Date.now() + 1000 * 60 * 60 * 24 * 7,
        maxAge: 1000 * 60 * 60 * 24 * 7
    }
}


app.use(session(sessionConfig));
app.use(helmet());

const scriptSrcUrls = [
    "https://stackpath.bootstrapcdn.com/",
    "https://kit.fontawesome.com/",
    "https://fonts.googleapis.com/",
    "https://cdnjs.cloudflare.com/",
    "https://cdn.jsdelivr.net"
];
const styleSrcUrls = [
    "https://kit-free.fontawesome.com/",
    "https://stackpath.bootstrapcdn.com/",
    "https://use.fontawesome.com/",
    "fonts.googleapis.com",
    "fonts.gstatic.com"
];
const connectSrcUrls = [
    "fonts.googleapis.com",
    "fonts.gstatic.com"
];
const fontSrcUrls = [
    "fonts.googleapis.com",
    "fonts.gstatic.com"
];
app.use(
    helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: [],
            connectSrc: ["'self'", ...connectSrcUrls],
            scriptSrc: ["'unsafe-inline'", "'self'", ...scriptSrcUrls],
            styleSrc: ["'self'", "'unsafe-inline'", ...styleSrcUrls],
            workerSrc: ["'self'", "blob:"],
            objectSrc: [],
            imgSrc: [
                "'self'",
                "blob:",
                "data:",
                "https://res.cloudinary.com/tetrayoubetra/", //SHOULD MATCH YOUR CLOUDINARY ACCOUNT! 
                "https://images.unsplash.com/",
            ],
            fontSrc: ["'self'", ...fontSrcUrls],
        },
    })
);

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use((req, res, next) => {
    res.locals.currentNav = '';
    res.locals.currentUser = req.user;
    res.locals.success = req.flash('success');
    res.locals.error = req.flash('error');
    next();
})

// check if banned
app.use(isBanned);


//
app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));

// GET home screen play view
app.get('/', catchAsync(async (req, res) => {
    res.render('home', { currentNav: "play" });
}));

app.use('/play', playRoutes);
app.use('/boards', boardsRoutes);
app.use('/boards/:id/comments', commentsRoutes)
app.use('/users', usersRoutes);
app.use('/admin', adminsRoutes);

// error handling middleware
app.all('*', (req, res, next) => {
    next(new ExpressError('Page Not Found', 404))
})

app.use((err, req, res, next) => {
    const { statusCode = 501 } = err;
    if (!err.message) err.message = 'Oh No, Something Went Wrong!'
    console.log(`Error at: ${req.originalUrl}`);
    console.log(`Method: ${req.method}`);
    console.log(err);
    res.status(statusCode).render('error', { err })
})

// port listeners always last
const PORT = process.env.PORT || 80;
app.listen(PORT, () => {
    console.log(`LISTENING ON PORT ${PORT}`)
});